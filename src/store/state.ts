import { Message } from "../actions/action";
import { Account } from "../domain/account";
import { Company } from "../domain/company";
import { Department } from "../domain/department";
import { Employee } from "../domain/employee";
import { ValidationError } from "../utils/apiUtils";

export type AccountState = {
    account?: Account;
    accountList: Account[];
};

export type ApiState = {
    errorMessages: Message[];
    numberOfApiGetInProgress: number;
    successMessages: Message[];
    validationError?: ValidationError;
};

export type CompanyState = {
    company?: Company;
    companyList: Company[];
};

export type DepartmentState = {
    department?: Department;
    departmentList: Department[];
};

export type EmployeeState = {
    employee?: Employee;
    employeeList: Employee[];
};
