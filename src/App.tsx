import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Template } from "smart-tech-solutions-frontend-components";
import "smart-tech-solutions-frontend-components/lib/Styles.css";

import AppMenu from "./AppMenu";
import CompanyListPage from "./containers/CompanyListPage";
import CompanyPage from "./containers/CompanyPage";
import DepartmentListPage from "./containers/DepartmentListPage";
import DepartmentPage from "./containers/DepartmentPage";
import EmployeeListPage from "./containers/EmployeeListPage";
import EmployeePage from "./containers/EmployeePage";
import HomePage from "./containers/HomePage";
import SettingsPage from "./containers/SettingsPage";

const menu = <AppMenu />;

const App = (): JSX.Element => (
    <>
        <Template currentLanguageCode="us" email="jonmic@gmail.com" fullName="Jonas Mičiulis" menu={menu} profileImagePath="/static/images/users/1.jpg">
            <Switch>
                <Route component={HomePage} exact={true} path="/" />
                <Route component={EmployeePage} path="/department/:departmentId/employee/:employeeId" />
                <Route component={DepartmentPage} path="/company/:companyId/department/:departmentId" />
                <Route component={CompanyPage} path="/company/:id" />
                <Route component={CompanyListPage} path="/company-list" />
                <Route component={DepartmentPage} path="/department/:id" />
                <Route component={DepartmentPage} path="/department" />
                <Route component={DepartmentListPage} path="/department-list" />
                <Route component={EmployeePage} path="/employee/:id" />
                <Route component={EmployeePage} path="/employee" />
                <Route component={EmployeeListPage} path="/employee-list" />
                <Route component={SettingsPage} path="/settings" />
                <Redirect to="/" />
            </Switch>
        </Template>
    </>
);

export default App;
