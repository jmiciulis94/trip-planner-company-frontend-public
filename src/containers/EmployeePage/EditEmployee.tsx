import { Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { FormActions, FormInput, FormOption, FormSection, FormSelect, FormValidationErrors, FormWrapper, Icon } from "smart-tech-solutions-frontend-components";
import { number, object, SchemaOf, string } from "yup";

import { Employee } from "../../domain/employee";
import { ValidationError } from "../../utils/apiUtils";

export type EditEmployeeProps = {
    departmentOptions: FormOption[];
    employee: Employee;
    onCancel: () => void;
    onSubmit: (values: Employee, actions: FormikHelpers<Employee>) => void;
    validationError?: ValidationError;
};

const employeeValidationSchema: SchemaOf<Employee> = object({
    cardId: number().notRequired().positive("Wrong number format"),
    departmentId: number().required("You must select an existing department").positive("You must select an existing department"),
    departmentName: string().notRequired(),
    firstName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    id: number().notRequired(),
    lastName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    phone: string().notRequired().max(20, "Can not be longer than 20 symbols")
});

const EditEmployee = ({ departmentOptions, employee, onCancel, onSubmit, validationError }: EditEmployeeProps): JSX.Element => {

    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={employee} validateOnBlur={false} validateOnChange={false} validationSchema={employeeValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount, handleSubmit }: FormikProps<Employee>) => (
                    <FormWrapper onSubmit={handleSubmit}>
                        <FormSection title="General">
                            <FormInput error={errors.firstName} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.firstName} label="First name" name="firstName" required={true} type="text" validated={submitCount > 0} value={values.firstName} onChange={setFieldValue} />
                            <FormInput error={errors.lastName} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.lastName} label="Last name" name="lastName" required={true} type="text" validated={submitCount > 0} value={values.lastName} onChange={setFieldValue} />
                            <FormInput error={errors.cardId} icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.cardId} label="Card number" name="cardId" required={false} type="text" validated={submitCount > 0} value={values.cardId} onChange={setFieldValue} />
                            <FormInput error={errors.phone} icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.phone} label="Phone number" name="phone" required={false} type="text" validated={submitCount > 0} value={values.phone} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Job">
                            <FormSelect error={errors.departmentId} invalid={!!errors.departmentId} label="Department" name="departmentId" options={departmentOptions} required={true} theme="floating" value={values.departmentId} onChange={setFieldValue} />
                        </FormSection>
                        <FormActions isSubmitting={isSubmitting} onCancel={onCancel} />
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default EditEmployee;
