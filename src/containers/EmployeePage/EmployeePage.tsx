import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";
import { BreadcrumbEntry, FormOption, Page } from "smart-tech-solutions-frontend-components";

import * as apiActions from "../../actions/apiActions";
import * as departmentActions from "../../actions/departmentActions";
import * as employeeActions from "../../actions/employeeActions";
import { getDepartmentOptions } from "../../domain/department";
import { Employee, fullName } from "../../domain/employee";
import { RootState } from "../../reducers/initialState";

import EditEmployee from "./EditEmployee";
import ViewEmployee from "./ViewEmployee";

export type EmployeePageProps = {
    departmentId: string;
    employeeId: string;
    id: string;
};

const mapStateToProps = (state: RootState) => {
    return {
        departmentList: state.department.departmentList,
        employee: state.employee.employee,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    getDepartmentList: departmentActions.getDepartmentList,
    getEmployee: employeeActions.getEmployee,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveEmployee: employeeActions.saveEmployee
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllEmployeePageProps = RouteComponentProps<EmployeePageProps> & PropsFromRedux;

const emptyEmployee: Employee = {
    departmentId: 0,
    firstName: "",
    lastName: ""
};

const EmployeePage = (props: AllEmployeePageProps): JSX.Element => {
    const [ departmentOptions, setDepartmentOptions ] = useState<FormOption[]>([]);
    const [ employee, setEmployee ] = useState<Employee>(emptyEmployee);
    const [ editMode, setEditMode ] = useState<boolean>(false);

    useEffect(() => {
        props.getDepartmentList();
    }, []);

    useEffect(() => {
        setDepartmentOptions(getDepartmentOptions(props.departmentList));
    }, [ props.departmentList ]);

    useEffect(() => {
        if (props.employee) {
            setEmployee(props.employee);
        }
    }, [ props.employee ]);

    useEffect(() => {
        const params: URLSearchParams = new URLSearchParams(props.location.search);
        const mode: string | null = params.get("mode");
        const employeeId: number = parseInt(props.match.params.employeeId);
        const id: number = parseInt(props.match.params.id);
        props.clearValidationError();

        if (id) {
            props.getEmployee(id);
            mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
        } else if (employeeId) {
            props.getEmployee(employeeId);
            mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
        } else {
            setEmployee(emptyEmployee);
            setEditMode(true);
        }
    }, [ props.match.params ]);

    const handleSubmit = async (employee: Employee, actions: FormikHelpers<Employee>): Promise<void> => {
        const success: boolean = await props.saveEmployee(employee);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
            props.history.goBack();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleCancel = (): void => {
        props.history.goBack();
    };

    const handleBack = (): void => {
        const employeeId: number = parseInt(props.match.params.employeeId);
        const id: number = parseInt(props.match.params.id);
        if (id) {
            props.history.push("/employee-list");
        } else if (employeeId) {
            props.history.push("/department-list");
        }
    };

    const handleEdit = (): void => {
        if (props.match.params.employeeId) {
            props.history.push("/employee/" + props.match.params.employeeId + "?mode=edit");
        } else {
            props.history.push("/employee/" + props.match.params.id + "?mode=edit");
        }
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Employees", url: "/employee-list" };
    const breadcrumbPage3: BreadcrumbEntry = { text: fullName(employee.firstName, employee.lastName), url: "/employee/" + employee.id };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2, breadcrumbPage3 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title={fullName(employee.firstName, employee.lastName)}>
            {editMode ? <EditEmployee departmentOptions={departmentOptions} employee={employee} validationError={props.validationError} onCancel={handleCancel} onSubmit={handleSubmit} /> : <ViewEmployee employee={employee} onBack={handleBack} onEdit={handleEdit} />}
        </Page>
    );
};

export default connector(EmployeePage);
