import React from "react";
import { Button, FormSection, FormValue, FormWrapper } from "smart-tech-solutions-frontend-components";

import { Employee } from "../../domain/employee";

export type ViewEmployeeProps = {
    employee: Employee;
    onBack: () => void;
    onEdit: () => void;
};

const ViewEmployee = ({ employee, onBack, onEdit }: ViewEmployeeProps): JSX.Element => (
    <FormWrapper theme="view">
        <FormSection title="General">
            <FormValue label="First name" value={employee.firstName} />
            <FormValue label="Last name" value={employee.lastName} />
            <FormValue label="Card number" value={employee.cardId ? employee.cardId : "No card number provided"} />
            <FormValue label="Phone number" value={employee.phone ? employee.phone : "No phone number provided"} />
        </FormSection>
        <FormSection title="Job">
            <FormValue label="Department name" value={employee.departmentName} />
        </FormSection>
        <Button text="Edit" theme="success" onClick={onEdit} />
        <Button text="Back" theme="secondary" onClick={onBack} />
    </FormWrapper>
);

export default ViewEmployee;
