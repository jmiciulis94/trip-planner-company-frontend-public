import React from "react";
import { Table, TableActions } from "smart-tech-solutions-frontend-components";

import { Employee, fullName } from "../../domain/employee";

export type EmployeeTableProps = {
    employeeList: Employee[];
    onDelete: (id: number) => void;
};

const EmployeeTable = ({ employeeList, onDelete }: EmployeeTableProps): JSX.Element => (
    <Table headers={[ "No", "Full Name", "Department name", "Actions" ]}>
        {employeeList.map((employee: Employee, i: number) => (
            <tr key={employee.id}>
                <td className="sts-table-numeric-column">{i + 1}</td>
                <td>{fullName(employee.firstName, employee.lastName)}</td>
                <td>{employee.departmentName}</td>
                <TableActions editUrl={"/employee/" + employee.id + "?mode=edit"} id={employee.id || 0} viewUrl={"/employee/" + employee.id + "?mode=view"} onDelete={onDelete} />
            </tr>
        ))}
    </Table>
);

export default EmployeeTable;
