import { Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { FormInput, FormModal, FormOption, FormSection, FormSelect, FormValidationErrors, Icon } from "smart-tech-solutions-frontend-components";
import { number, object, SchemaOf, string } from "yup";

import { Employee } from "../../domain/employee";
import { ValidationError } from "../../utils/apiUtils";

export type ViewEmployeeProps = {
    departmentOptions: FormOption[];
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (values: Employee, actions: FormikHelpers<Employee>) => void;
    validationError?: ValidationError;
};

const employeeValidationSchema: SchemaOf<Employee> = object({
    cardId: number().notRequired().positive("Wrong number format"),
    departmentId: number().required("You must select an existing department").positive("You must select an existing department"),
    departmentName: string().notRequired(),
    firstName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    id: number().notRequired(),
    lastName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    phone: string().notRequired().max(20, "Can not be longer than 20 symbols")
});

const emptyEmployee = {
    departmentId: 0,
    firstName: "",
    lastName: ""
};

const CreateEmployee = ({ departmentOptions, isOpen, onClose, onSubmit, validationError }: ViewEmployeeProps): JSX.Element => (
    <>
        <FormValidationErrors validationError={validationError} />
        <Formik enableReinitialize={true} initialValues={emptyEmployee} validateOnBlur={false} validateOnChange={false} validationSchema={employeeValidationSchema} onSubmit={onSubmit}>
            {({ values, errors, isSubmitting, setFieldValue, submitCount, handleSubmit }: FormikProps<Employee>) => (
                <FormModal isOpen={isOpen} isSubmitting={isSubmitting} title="Create New Employee" onClose={onClose} onSubmit={handleSubmit}>
                    <FormSection title="General">
                        <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.firstName} label="First name" name="firstName" required={true} type="text" validated={submitCount > 0} value={values.firstName} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.lastName} label="Last name" name="lastName" required={true} type="text" validated={submitCount > 0} value={values.lastName} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.cardId} label="Card number" name="cardId" required={false} type="text" validated={submitCount > 0} value={values.cardId} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.phone} label="Phone number" name="phone" required={false} type="text" validated={submitCount > 0} value={values.phone} onChange={setFieldValue} />
                    </FormSection>
                    <FormSection title="Job">
                        <FormSelect invalid={!!errors.departmentId} label="Department" name="departmentId" options={departmentOptions} required={true} value={values.departmentId} onChange={setFieldValue} />
                    </FormSection>
                </FormModal>
            )}
        </Formik>
    </>
);

export default CreateEmployee;
