import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, FormOption, Page, showDeleteAlert } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as departmentActions from "../../actions/departmentActions";
import * as employeeActions from "../../actions/employeeActions";
import { getDepartmentOptions } from "../../domain/department";
import { Employee } from "../../domain/employee";
import { RootState } from "../../reducers/initialState";

import CreateEmployee from "./CreateEmployee";
import EmployeeTable from "./EmployeeTable";

const mapStateToProps = (state: RootState) => {
    return {
        departmentList: state.department.departmentList,
        employeeList: state.employee.employeeList,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    deleteEmployee: employeeActions.deleteEmployee,
    getDepartmentList: departmentActions.getDepartmentList,
    getEmployeeList: employeeActions.getEmployeeList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveEmployee: employeeActions.saveEmployee
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const EmployeeListPage = (props: PropsFromRedux): JSX.Element => {
    const [ departmentOptions, setDepartmentOptions ] = useState<FormOption[]>([]);
    const [ isModalOpen, setIsModalOpen ] = useState<boolean>(false);

    useEffect(() => {
        props.getDepartmentList();
        props.getEmployeeList();
    }, []);

    useEffect(() => {
        props.getDepartmentList();
        props.getEmployeeList();
    }, [ props.getDepartmentList ]);

    useEffect(() => {
        setDepartmentOptions(getDepartmentOptions(props.departmentList));
    }, [ props.departmentList ]);

    const handleOpenModal = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
    };

    const handleSubmit = async (employee: Employee, actions: FormikHelpers<Employee>): Promise<void> => {
        const success: boolean = await props.saveEmployee(employee);
        setIsModalOpen(false);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleDelete = async (id: number): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert();
        if (result.value) {
            await props.deleteEmployee(id);
            window.scrollTo(0, 0);
        }
    };

    const renderNoEmployeesMessage = (): JSX.Element => (
        <>You don&#39;t have any employees yet.</>
    );

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Employees", url: "/employee-list" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Employees" onCreate={handleOpenModal}>
            {props.employeeList.length > 0 ? <EmployeeTable employeeList={props.employeeList} onDelete={handleDelete} /> : renderNoEmployeesMessage()}
            <CreateEmployee departmentOptions={departmentOptions} isOpen={isModalOpen} onClose={handleCloseModal} onSubmit={handleSubmit} />
        </Page>
    );
};

export default connector(EmployeeListPage);
