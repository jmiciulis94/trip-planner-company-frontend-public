import React from "react";
import { Button, FormSection, FormValue, FormWrapper, Table } from "smart-tech-solutions-frontend-components";

import { Company } from "../../domain/company";
import { Department } from "../../domain/department";

export type ViewCompanyProps = {
    company: Company;
    onBack: () => void;
    onEdit: () => void;
};

const renderIfCompanyHasDepartments = (company: Company): JSX.Element => {
    if (company.departments && company.departments.length > 0) {
        return (
            <Table headers={[ "Department name" ]}>
                {company.departments.map((department: Department) =>
                    <tr key={department.id}>
                        <td>{department.name}</td>
                    </tr>
                )}
            </Table>
        );
    } else {
        return (
            <>Company doesn&#39;t have any departments yet.<br /><br /></>
        );
    }
};

const ViewCompany = ({ onBack, onEdit, company }: ViewCompanyProps): JSX.Element => (
    <FormWrapper theme="view">
        <FormSection title="General">
            <FormValue label="Company name" value={company.name} />
            <FormValue label="Company code" value={company.companyCode} />
            <FormValue label="VAT code" value={company.vatCode} />
        </FormSection>
        <FormSection title="Contacts">
            <FormValue label="City" value={company.city} />
            <FormValue label="Address line" value={company.address} />
            <FormValue label="Postal code" value={company.postalCode ? company.postalCode : "not provided"} />
            <FormValue label="Email" value={company.email} />
            <FormValue label="Phone number" value={company.phone} />
        </FormSection>
        <FormSection title="Departments">
            {renderIfCompanyHasDepartments(company)}
        </FormSection>
        <Button text="Edit" theme="success" onClick={onEdit} />
        <Button text="Back" theme="secondary" onClick={onBack} />
    </FormWrapper>
);

export default ViewCompany;
