import { ArrayHelpers, FormikErrors, getIn } from "formik";
import React from "react";
import { FormInput, Icon } from "smart-tech-solutions-frontend-components";

import RemoveButton from "../../components/RemoveButton";
import { Company } from "../../domain/company";

export type DepartmentFieldsProps = {
    arrayHelpers: ArrayHelpers;
    errors?: FormikErrors<Company>;
    index: number;
    onRemove: (id: number, arrayHelpers: ArrayHelpers) => void;
    setFieldValue: (name: string, value: string | number | boolean | null | undefined) => void;
    submitCount: number;
    values: Company;
};

const DepartmentFields = ({ arrayHelpers, errors, index, onRemove, setFieldValue, submitCount, values }: DepartmentFieldsProps): JSX.Element => {

    return (
        <tr key={index}>
            <td>
                <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!getIn(errors, "departments." + index + ".name")} name={`departments.${index}.name`} theme="no-margins" type="text" validated={submitCount > 0} value={values.departments && values.departments[index].name} onChange={setFieldValue} />
            </td>
            {values.departments && values.departments[index].id ? <RemoveButton arrayHelpers={arrayHelpers} id={index} onRemove={onRemove} /> : <RemoveButton arrayHelpers={arrayHelpers} id={index} onRemove={() => {
                arrayHelpers.remove(index);
            }} />}
        </tr>
    );
};

export default DepartmentFields;
