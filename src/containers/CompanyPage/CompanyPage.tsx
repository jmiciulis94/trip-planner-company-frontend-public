import { ArrayHelpers, FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";
import { BreadcrumbEntry, Page } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as companyActions from "../../actions/companyActions";
import { showDeleteAlert } from "../../components/Alert";
import { Company } from "../../domain/company";
import { RootState } from "../../reducers/initialState";

import EditCompany from "./EditCompany";
import ViewCompany from "./ViewCompany";

export type CompanyPageProps = {
    id: string;
};

const mapStateToProps = (state: RootState) => {
    return {
        company: state.company.company,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    getCompany: companyActions.getCompany,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveCompany: companyActions.saveCompany
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllCompanyPageProps = RouteComponentProps<CompanyPageProps> & PropsFromRedux;

const emptyCompany = {
    address: "",
    city: "",
    companyCode: 0,
    departments: [],
    email: "",
    name: "",
    phone: "",
    vatCode: 0
};

const CompanyPage = (props: AllCompanyPageProps): JSX.Element => {
    const [ company, setCompany ] = useState<Company>(emptyCompany);
    const [ editMode, setEditMode ] = useState<boolean>(false);

    useEffect(() => {
        if (props.company) {
            setCompany(props.company);
        }
    }, [ props.company ]);

    useEffect(() => {
        const params: URLSearchParams = new URLSearchParams(props.location.search);
        const mode: string | null = params.get("mode");
        const id: number = parseInt(props.match.params.id);
        props.clearValidationError();

        if (id) {
            props.getCompany(id);
            mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
        } else {
            setCompany(emptyCompany);
            setEditMode(true);
        }
    }, [ props.match.params ]);

    const handleRemove = async (id: number, arrayHelpers: ArrayHelpers): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert("remove");
        if (result.value) {
            arrayHelpers.remove(id);
            window.scrollTo(0, 0);
        }
    };

    const handleSubmit = async (company: Company, actions: FormikHelpers<Company>): Promise<void> => {
        const success: boolean = await props.saveCompany(company);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
            props.history.goBack();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleBack = (): void => {
        props.history.goBack();
    };

    const handleEdit = (): void => {
        props.history.push("/company/" + props.match.params.id + "?mode=edit");
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Companies", url: "/company-list" };
    const breadcrumbPage3: BreadcrumbEntry = { text: company.name, url: "/company/" + company.id };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2, breadcrumbPage3 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title={company.name}>
            {editMode ? <EditCompany company={company} validationError={props.validationError} onCancel={handleBack} onRemove={handleRemove} onSubmit={handleSubmit} /> : <ViewCompany company={company} onBack={handleBack} onEdit={handleEdit} />}
        </Page>
    );
};

export default connector(CompanyPage);
