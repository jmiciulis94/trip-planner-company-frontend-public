import { ArrayHelpers, FieldArray, FieldArrayRenderProps, Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { Button, FormActions, FormInput, FormSection, FormValidationErrors, FormWrapper, Icon, Table } from "smart-tech-solutions-frontend-components";
import { array, boolean, number, object, SchemaOf, string } from "yup";

import { Company } from "../../domain/company";
import { Department } from "../../domain/department";
import { ValidationError } from "../../utils/apiUtils";

import DepartmentFields from "./DepartmentFields";

export type EditCompanyProps = {
    company: Company;
    onCancel: () => void;
    onRemove: (id: number, arrayHelpers: ArrayHelpers) => void;
    onSubmit: (values: Company, actions: FormikHelpers<Company>) => void;
    validationError?: ValidationError;
};

const companyValidationSchema: SchemaOf<Company> = object({
    address: string().required("This field is required").max(200, "Can not be longer than 200 symbols"),
    city: string().required("This field is required").max(200, "Can not be longer than 200 symbols"),
    companyCode: number().required("This field is required").positive("Wrong number format"),
    departments: array().of(
        object().shape({
            companyId: number().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            companyName: string().notRequired(),
            deleted: boolean().notRequired(),
            employees: array().of(
                object().shape({
                    cardId: number().notRequired().positive("Wrong number format"),
                    deleted: boolean().notRequired(),
                    departmentId: number().required("This field is required").max(100, "Can not be longer than 100 symbols"),
                    departmentName: string().notRequired(),
                    firstName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
                    id: number().notRequired(),
                    lastName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
                    phone: string().notRequired()
                }).notRequired()
            ),
            id: number().notRequired(),
            name: string().required("This field is required").max(100, "Can not be longer than 100 symbols")
        }).notRequired()
    ),
    email: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    id: number().notRequired(),
    name: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    phone: string().required("This field is required").max(20, "Can not be longer than 20 symbols"),
    postalCode: string().notRequired(),
    vatCode: number().required("This field is required").positive("Wrong number format")
});

const EditCompany = ({ onCancel, onRemove, onSubmit, company, validationError }: EditCompanyProps): JSX.Element => {
    const addDepartment = (): Department => {
        return {
            companyId: company.id || 0,
            name: ""
        };
    };

    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={company} validateOnBlur={false} validateOnChange={false} validationSchema={companyValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount, handleSubmit }: FormikProps<Company>) => (
                    <FormWrapper onSubmit={handleSubmit}>
                        <FormSection title="General">
                            <FormInput error={errors.name} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.name} label="Company name" name="name" required={true} type="text" validated={submitCount > 0} value={values.name} onChange={setFieldValue} />
                            <FormInput error={errors.companyCode} icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.companyCode} label="Company code" name="companyCode" required={true} type="text" validated={submitCount > 0} value={values.companyCode} onChange={setFieldValue} />
                            <FormInput error={errors.vatCode} icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.vatCode} label="VAT code" name="vatCode" required={true} type="text" validated={submitCount > 0} value={values.vatCode} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Contacts">
                            <FormInput error={errors.city} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.city} label="City" name="city" required={true} type="text" validated={submitCount > 0} value={values.city} onChange={setFieldValue} />
                            <FormInput error={errors.address} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.address} label="Address line" name="address" required={true} type="text" validated={submitCount > 0} value={values.address} onChange={setFieldValue} />
                            <FormInput error={errors.postalCode} icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.postalCode} label="Postal code" name="postalCode" required={false} type="text" validated={submitCount > 0} value={values.postalCode} onChange={setFieldValue} />
                            <FormInput error={errors.email} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.email} label="Email" name="email" required={true} type="text" validated={submitCount > 0} value={values.email} onChange={setFieldValue} />
                            <FormInput error={errors.phone} icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.phone} label="Phone number" name="phone" required={true} type="text" validated={submitCount > 0} value={values.phone} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Departments">
                            <FieldArray name="departments">
                                {(arrayHelpers: FieldArrayRenderProps) => (
                                    <>
                                        <Table headers={[ "Department name", "Remove" ]} required={[ true ]}>
                                            {values.departments && values.departments.map((department: Department, index: number) =>
                                                <DepartmentFields arrayHelpers={arrayHelpers} errors={errors} index={index} key={department.id} setFieldValue={setFieldValue} submitCount={submitCount} values={values} onRemove={onRemove} />
                                            )}
                                        </Table>
                                        <Button text="+ Add Department" theme="success" onClick={() => arrayHelpers.push(addDepartment())} />
                                    </>
                                )}
                            </FieldArray>
                        </FormSection>
                        <FormActions isSubmitting={isSubmitting} onCancel={onCancel} />
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default EditCompany;
