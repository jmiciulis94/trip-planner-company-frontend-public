import { FormikHelpers } from "formik";
import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";

import * as accountActions from "../../actions/accountActions";
import * as apiActions from "../../actions/apiActions";
import LoginWrapper from "../../components/LoginWrapper";
import SignUp from "../../components/LoginWrapper/SignUp";
import { Account } from "../../domain/account";
import { RootState } from "../../reducers/initialState";

import SignIn from "./SignIn";

const mapStateToProps = (state: RootState) => {
    return {
        account: state.account.account,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveAccount: accountActions.saveAccount
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllLoginPageProps = RouteComponentProps & PropsFromRedux;

const LoginPage = (props: AllLoginPageProps): JSX.Element => {

    const handleSubmit = async (account: Account, actions: FormikHelpers<Account>): Promise<void> => {
        const success: boolean = await props.saveAccount(account); // toDo: create Login account

        if (success) {
            actions.resetForm();
            props.clearValidationError();
            props.history.push("/");
        }
    };

    return (
        <LoginWrapper backgroundImagePath="../../../public/images/background/vejas-background.jpg">
            <SignIn validationError={props.validationError} onSubmit={handleSubmit} />
            <SignUp />
        </LoginWrapper>
    );
};

export default connector(LoginPage);
