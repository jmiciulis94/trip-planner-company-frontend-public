import { Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { FormInput, FormSection, FormValidationErrors, FormWrapper, Icon } from "smart-tech-solutions-frontend-components";
import { number, object, SchemaOf, string } from "yup";

import { Account } from "../../domain/account";
import { ValidationError } from "../../utils/apiUtils";

import LoginButton from "./LoginButton";
import RememberMe from "./RememberMe";

export type SignInProps = {
    onSubmit: (values: Account, actions: FormikHelpers<Account>) => void;
    validationError?: ValidationError;
};

const loginValidationSchema: SchemaOf<Account> = object({
    avatar: string().notRequired(),
    email: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    firstName: string().notRequired(),
    id: number().notRequired(),
    lastName: string().notRequired(),
    password: string().required("No password provided.").min(8, "Password is too short - should be 8 chars minimum.").matches(/[a-zA-Z]/, "Password can only contain Latin letters."),
    phone: string().notRequired(),
    role: string().notRequired()
});

const emptyLogin = {
    email: "",
    password: ""
};

const SignIn = ({ onSubmit, validationError }: SignInProps): JSX.Element => {

    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={emptyLogin} validateOnBlur={false} validateOnChange={false} validationSchema={loginValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount, handleSubmit }: FormikProps<Account>) => (
                    <FormWrapper onSubmit={handleSubmit}>
                        <FormSection title="Sign In">
                            <FormInput error={errors.email} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.email} label="Email" name="email" placeholder="Email" type="text" validated={submitCount > 0} value={values.email} onChange={setFieldValue} />
                            <FormInput error={errors.password} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.password} label="Password" name="password" placeholder="Password" type="password" validated={submitCount > 0} value={values.password} onChange={setFieldValue} />
                        </FormSection>
                        <RememberMe />
                        <LoginButton isSubmitting={isSubmitting} />
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default SignIn;
