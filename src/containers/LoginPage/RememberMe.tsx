import React from "react";
import Form from "react-bootstrap/Form";

import "../../components/LoginWrapper/LoginWrapper.css";

const RememberMe = (): JSX.Element => {

    return (
        <Form.Group>
            <div className="col-md-12">
                <div className="checkbox checkbox-primary float-left pt-0">
                    <input id="checkbox-signup" type="checkbox" />
                    <label htmlFor="checkbox-signup">Remember me</label>
                </div>
                <a className="text-dark float-right" href="forgot-password"><i className="fa fa-lock mr-1" /> Forgot pwd?</a>
            </div>
        </Form.Group>
    );
};

export default RememberMe;
