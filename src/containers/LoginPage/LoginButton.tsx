import React from "react";

import "../../components/LoginWrapper/LoginWrapper.css";

export type LoginButtonProps = {
    isSubmitting: boolean;
};

const LoginButton = ({ isSubmitting }: LoginButtonProps): JSX.Element => {

    return (
        <>
            <div className="form-group text-center mt-3">
                <div className="col-xs-12">
                    <button className="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" disabled={isSubmitting} type="submit">{isSubmitting ? "Logging in..." : "Log In"}</button>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 mt-2 text-center">
                    <div className="social">
                        <a className="btn btn-facebook" data-toggle="tooltip" href="javascript:void(0)" title="LoginButton with Facebook"> <i aria-hidden="true" className="fab fa-facebook" /> </a>
                        <a className="btn btn-googleplus" data-toggle="tooltip" href="javascript:void(0)" title="LoginButton with Google"> <i aria-hidden="true" className="fab fa-google-plus" /> </a>
                    </div>
                </div>
            </div>
        </>
    );
};

export default LoginButton;
