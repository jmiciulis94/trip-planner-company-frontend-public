import { Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { FormInput, FormModal, FormSection, FormValidationErrors, Icon } from "smart-tech-solutions-frontend-components";
import { array, boolean, number, object, SchemaOf, string } from "yup";

import { Company } from "../../domain/company";
import { ValidationError } from "../../utils/apiUtils";

export type ViewCompanyProps = {
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (values: Company, actions: FormikHelpers<Company>) => void;
    validationError?: ValidationError;
};

const companyValidationSchema: SchemaOf<Company> = object({
    address: string().required("This field is required").max(200, "Can not be longer than 200 symbols"),
    city: string().required("This field is required").max(200, "Can not be longer than 200 symbols"),
    companyCode: number().required("This field is required").positive("Wrong number format"),
    departments: array().of(
        object().shape({
            companyId: number().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            companyName: string().notRequired(),
            deleted: boolean().notRequired(),
            employees: array().of(
                object().shape({
                    cardId: number().notRequired().positive("Wrong number format"),
                    deleted: boolean().notRequired(),
                    departmentId: number().required("This field is required").max(100, "Can not be longer than 100 symbols"),
                    departmentName: string().notRequired(),
                    firstName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
                    id: number().notRequired(),
                    lastName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
                    phone: string().notRequired()
                }).notRequired()
            ),
            id: number().notRequired(),
            name: string().required("This field is required").max(100, "Can not be longer than 100 symbols")
        }).notRequired()
    ),
    email: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    id: number().notRequired(),
    name: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
    phone: string().required("This field is required").max(20, "Can not be longer than 20 symbols"),
    postalCode: string().notRequired(),
    vatCode: number().required("This field is required").positive("Wrong number format")
});

const emptyCompany = {
    address: "",
    city: "",
    companyCode: "",
    departments: [],
    email: "",
    name: "",
    phone: "",
    vatCode: ""
};

const CreateCompany = ({ isOpen, onClose, onSubmit, validationError }: ViewCompanyProps): JSX.Element => (
    <>
        <FormValidationErrors validationError={validationError} />
        <Formik enableReinitialize={true} initialValues={emptyCompany} validateOnBlur={false} validateOnChange={false} validationSchema={companyValidationSchema} onSubmit={onSubmit}>
            {({ values, errors, isSubmitting, setFieldValue, submitCount, handleSubmit }: FormikProps<Company>) => (
                <FormModal isOpen={isOpen} isSubmitting={isSubmitting} title="Create New Company" onClose={onClose} onSubmit={handleSubmit}>
                    <FormSection title="General">
                        <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.name} label="Company name" name="name" required={true} type="text" validated={submitCount > 0} value={values.name} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.companyCode} label="Company code" name="companyCode" required={true} type="text" validated={submitCount > 0} value={values.companyCode} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.vatCode} label="VAT code" name="vatCode" required={true} type="text" validated={submitCount > 0} value={values.vatCode} onChange={setFieldValue} />
                    </FormSection>
                    <FormSection title="Contacts">
                        <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.city} label="City" name="city" required={true} type="text" validated={submitCount > 0} value={values.city} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.address} label="Address line" name="address" required={true} type="text" validated={submitCount > 0} value={values.address} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.postalCode} label="Postal code" name="postalCode" required={false} type="text" validated={submitCount > 0} value={values.postalCode} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.email} label="Email" name="email" required={true} type="text" validated={submitCount > 0} value={values.email} onChange={setFieldValue} />
                        <FormInput icon={<Icon iconClass="pencil-alt" type="fa" />} invalid={!!errors.phone} label="Phone number" name="phone" required={true} type="text" validated={submitCount > 0} value={values.phone} onChange={setFieldValue} />
                    </FormSection>
                </FormModal>
            )}
        </Formik>
    </>
);

export default CreateCompany;
