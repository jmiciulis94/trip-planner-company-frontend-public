import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, Page, showDeleteAlert } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as companyActions from "../../actions/companyActions";
import { Company } from "../../domain/company";
import { RootState } from "../../reducers/initialState";

import CompanyTable from "./CompanyTable";
import CreateCompany from "./CreateCompany";

const mapStateToProps = (state: RootState) => {
    return {
        companyList: state.company.companyList,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    deleteCompany: companyActions.deleteCompany,
    getCompanyList: companyActions.getCompanyList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveCompany: companyActions.saveCompany
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const CompanyListPage = (props: PropsFromRedux): JSX.Element => {
    const [ isModalOpen, setIsModalOpen ] = useState<boolean>(false);

    useEffect(() => {
        props.getCompanyList();
    }, []);

    const handleOpenModal = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
    };

    const handleSubmit = async (company: Company, actions: FormikHelpers<Company>): Promise<void> => {
        const success: boolean = await props.saveCompany(company);
        setIsModalOpen(false);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleDelete = async (id: number): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert();
        if (result.value) {
            await props.deleteCompany(id);
            window.scrollTo(0, 0);
        }
    };

    const renderNoCompaniesMessage = (): JSX.Element => (
        <>You don&#39;t have any companies yet.</>
    );

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Companies", url: "/company-list" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <>
            <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Companies" onCreate={handleOpenModal}>
                {props.companyList.length > 0 ? <CompanyTable companyList={props.companyList} onDelete={handleDelete} /> : renderNoCompaniesMessage()}
            </Page>
            <CreateCompany isOpen={isModalOpen} onClose={handleCloseModal} onSubmit={handleSubmit} />
        </>
    );
};

export default connector(CompanyListPage);
