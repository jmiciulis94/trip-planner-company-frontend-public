import React from "react";
import { Link } from "react-router-dom";
import { Table, TableActions } from "smart-tech-solutions-frontend-components";

import { Company, fullAddress } from "../../domain/company";
import { Department } from "../../domain/department";

export type CompanyTableProps = {
    companyList: Company[];
    onDelete: (id: number) => void;
};

const companyHasNoDepartments = (): JSX.Element => (
    <>Company has no departments.</>
);

const CompanyTable = ({ onDelete, companyList }: CompanyTableProps): JSX.Element => (
    <Table headers={[ "No", "Company name", "Address", "Departments", "Actions" ]}>
        {companyList.map((company: Company, i: number) => (
            <tr key={company.id}>
                <td className="sts-table-numeric-column">{i + 1}</td>
                <td>{company.name}</td>
                <td>{fullAddress(company.address, company.city)}</td>
                <td>
                    {company.departments && company.departments.length > 0 ? company.departments.map((department: Department, i: number) => (
                        <React.Fragment key={department.id}>
                            {i !== 0 && <br />}
                            <Link to={"/company/" + company.id + "/department/" + department.id}>{department.name}</Link>
                        </React.Fragment>
                    )) : companyHasNoDepartments()}
                </td>
                <TableActions editUrl={"/company/" + company.id + "?mode=edit"} id={company.id || 0} viewUrl={"/company/" + company.id + "?mode=view"} onDelete={onDelete} />
            </tr>
        ))}
    </Table>
);

export default CompanyTable;
