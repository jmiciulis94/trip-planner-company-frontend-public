import { Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { FormInput, FormModal, FormOption, FormSection, FormSelect, FormValidationErrors, Icon } from "smart-tech-solutions-frontend-components";
import { array, boolean, number, object, SchemaOf, string } from "yup";

import { Department } from "../../domain/department";
import { ValidationError } from "../../utils/apiUtils";

export type ViewDepartmentProps = {
    companyOptions: FormOption[];
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (values: Department, actions: FormikHelpers<Department>) => void;
    validationError?: ValidationError;
};

const departmentValidationSchema: SchemaOf<Department> = object({
    companyId: number().required("You must select an existing company").positive("You must select an existing company"),
    companyName: string().notRequired(),
    employees: array().of(
        object().shape({
            cardId: number().notRequired().positive("Wrong number format"),
            deleted: boolean().notRequired(),
            departmentId: number().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            departmentName: string().notRequired(),
            firstName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            id: number().notRequired(),
            lastName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            phone: string().notRequired()
        }).notRequired()
    ),
    id: number().notRequired(),
    name: string().required("This field is required").max(100, "Can not be longer than 100 symbols")
});

const emptyDepartment = {
    companyId: 0,
    employees: [],
    name: ""
};

const CreateDepartment = ({ companyOptions, isOpen, onClose, onSubmit, validationError }: ViewDepartmentProps): JSX.Element => (
    <>
        <FormValidationErrors validationError={validationError} />
        <Formik enableReinitialize={true} initialValues={emptyDepartment} validateOnBlur={false} validateOnChange={false} validationSchema={departmentValidationSchema} onSubmit={onSubmit}>
            {({ values, errors, isSubmitting, setFieldValue, submitCount, handleSubmit }: FormikProps<Department>) => (
                <FormModal isOpen={isOpen} isSubmitting={isSubmitting} title="Create New Department" onClose={onClose} onSubmit={handleSubmit}>
                    <FormSection title="General">
                        <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.name} label="Department name" name="name" required={true} type="text" validated={submitCount > 0} value={values.name} onChange={setFieldValue} />
                    </FormSection>
                    <FormSection title="Job">
                        <FormSelect invalid={!!errors.companyId} label="Company" name="companyId" options={companyOptions} required={true} value={values.companyId} onChange={setFieldValue} />
                    </FormSection>
                </FormModal>
            )}
        </Formik>
    </>
);

export default CreateDepartment;
