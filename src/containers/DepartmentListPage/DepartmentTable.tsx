import React from "react";
import { Link } from "react-router-dom";
import { Table, TableActions } from "smart-tech-solutions-frontend-components";

import { Department } from "../../domain/department";
import { Employee, fullName } from "../../domain/employee";

export type DepartmentTableProps = {
    departmentList: Department[];
    onDelete: (id: number) => void;
};

const departmentHasNoEmployees = (): JSX.Element => (
    <>Department has no employees.</>
);

const DepartmentTable = ({ onDelete, departmentList }: DepartmentTableProps): JSX.Element => (
    <Table headers={[ "No", "Department name", "Employees", "Actions" ]}>
        {departmentList.map((department: Department, i: number) => (
            <tr key={department.id}>
                <td className="sts-table-numeric-column">{i + 1}</td>
                <td>{department.name}</td>
                <td>
                    {department.employees && department.employees.length > 0 ? department.employees.map((employee: Employee, i: number) => (
                        <React.Fragment key={employee.id}>
                            {i !== 0 && <br />}
                            <Link to={"/department/" + department.id + "/employee/" + employee.id}>{fullName(employee.firstName, employee.lastName)}</Link>
                        </React.Fragment>
                    )) : departmentHasNoEmployees()}
                </td>
                <TableActions editUrl={"/department/" + department.id + "?mode=edit"} id={department.id || 0} viewUrl={"/department/" + department.id + "?mode=view"} onDelete={onDelete} />
            </tr>
        ))}
    </Table>
);

export default DepartmentTable;
