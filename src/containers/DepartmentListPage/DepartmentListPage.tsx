import { FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BreadcrumbEntry, FormOption, Page, showDeleteAlert } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as companyActions from "../../actions/companyActions";
import * as departmentActions from "../../actions/departmentActions";
import { getCompanyOptions } from "../../domain/company";
import { Department } from "../../domain/department";
import { RootState } from "../../reducers/initialState";

import CreateDepartment from "./CreateDepartment";
import DepartmentTable from "./DepartmentTable";

const mapStateToProps = (state: RootState) => {
    return {
        companyList: state.company.companyList,
        departmentList: state.department.departmentList,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    deleteDepartment: departmentActions.deleteDepartment,
    getCompanyList: companyActions.getCompanyList,
    getDepartmentList: departmentActions.getDepartmentList,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveDepartment: departmentActions.saveDepartment
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

const DepartmentListPage = (props: PropsFromRedux): JSX.Element => {
    const [ companyOptions, setCompanyOptions ] = useState<FormOption[]>([]);
    const [ isModalOpen, setIsModalOpen ] = useState<boolean>(false);

    useEffect(() => {
        props.getCompanyList();
        props.getDepartmentList();
    }, []);

    useEffect(() => {
        props.getCompanyList();
        props.getDepartmentList();
    }, [ props.getCompanyList ]);

    useEffect(() => {
        setCompanyOptions(getCompanyOptions(props.companyList));
    }, [ props.companyList ]);

    const handleOpenModal = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
    };

    const handleSubmit = async (department: Department, actions: FormikHelpers<Department>): Promise<void> => {
        const success: boolean = await props.saveDepartment(department);
        setIsModalOpen(false);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleDelete = async (id: number): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert();
        if (result.value) {
            await props.deleteDepartment(id);
            window.scrollTo(0, 0);
        }
    };

    const renderNoDepartmentsMessage = (): JSX.Element => (
        <>You don&#39;t have any departments yet.</>
    );

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Departments", url: "/department-list" };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2 ];

    return (
        <>
            <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title="Departments" onCreate={handleOpenModal}>
                {props.departmentList.length > 0 ? <DepartmentTable departmentList={props.departmentList} onDelete={handleDelete} /> : renderNoDepartmentsMessage()}
            </Page>
            <CreateDepartment companyOptions={companyOptions} isOpen={isModalOpen} onClose={handleCloseModal} onSubmit={handleSubmit} />
        </>
    );
};

export default connector(DepartmentListPage);
