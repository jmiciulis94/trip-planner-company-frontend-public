import React from "react";
import { Button, FormSection, FormValue, FormWrapper, Table } from "smart-tech-solutions-frontend-components";

import { Department } from "../../domain/department";
import { Employee } from "../../domain/employee";

export type ViewDepartmentProps = {
    department: Department;
    onBack: () => void;
    onEdit: () => void;
};

const renderIfDepartmentHasEmployees = (department: Department): JSX.Element => {
    if (department.employees && department.employees.length > 0) {
        return (
            <Table headers={[ "First name", "Last name" ]}>
                {department.employees.map((employee: Employee) =>
                    <tr key={employee.id}>
                        <td>{employee.firstName}</td>
                        <td>{employee.lastName}</td>
                    </tr>
                )}
            </Table>
        );
    } else {
        return (
            <>Department doesn&#39;t have any employees yet.<br /><br /></>
        );
    }
};

const ViewCompany = ({ onBack, onEdit, department }: ViewDepartmentProps): JSX.Element => (
    <FormWrapper theme="view">
        <FormSection title="General">
            <FormValue label="Name" value={department.name} />
        </FormSection>
        <FormSection title="Employees">
            {renderIfDepartmentHasEmployees(department)}
        </FormSection>
        <Button text="Edit" theme="success" onClick={onEdit} />
        <Button text="Back" theme="secondary" onClick={onBack} />
    </FormWrapper>
);

export default ViewCompany;
