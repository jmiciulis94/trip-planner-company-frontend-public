import { ArrayHelpers, FieldArray, FieldArrayRenderProps, Formik, FormikProps } from "formik";
import { FormikHelpers } from "formik/dist/types";
import React from "react";
import { Button, FormActions, FormInput, FormOption, FormSection, FormSelect, FormValidationErrors, FormWrapper, Icon, Table } from "smart-tech-solutions-frontend-components";
import { array, boolean, number, object, SchemaOf, string } from "yup";

import { Department } from "../../domain/department";
import { Employee } from "../../domain/employee";
import { ValidationError } from "../../utils/apiUtils";

import EmployeeFields from "./EmployeeFields";

export type EditDepartmentProps = {
    companyOptions: FormOption[];
    department: Department;
    onCancel: () => void;
    onRemove: (id: number, arrayHelpers: ArrayHelpers) => void;
    onSubmit: (values: Department, actions: FormikHelpers<Department>) => void;
    validationError?: ValidationError;
};

const departmentValidationSchema: SchemaOf<Department> = object({
    companyId: number().required("You must select an existing company").positive("You must select an existing company"),
    companyName: string().notRequired(),
    employees: array().of(
        object().shape({
            cardId: number().notRequired().positive("Wrong number format"),
            deleted: boolean().notRequired(),
            departmentId: number().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            departmentName: string().notRequired(),
            firstName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            id: number().notRequired(),
            lastName: string().required("This field is required").max(100, "Can not be longer than 100 symbols"),
            phone: string().notRequired()
        }).notRequired()
    ),
    id: number().notRequired(),
    name: string().required("This field is required").max(100, "Can not be longer than 100 symbols")
});

const EditDepartment = ({ companyOptions, onCancel, onRemove, onSubmit, department, validationError }: EditDepartmentProps): JSX.Element => {
    const addEmployee = (): Employee => {
        return {
            departmentId: department.id || 0,
            firstName: "",
            lastName: ""
        };
    };

    return (
        <>
            <FormValidationErrors validationError={validationError} />
            <Formik enableReinitialize={true} initialValues={department} validateOnBlur={false} validateOnChange={false} validationSchema={departmentValidationSchema} onSubmit={onSubmit}>
                {({ values, errors, isSubmitting, setFieldValue, submitCount, handleSubmit }: FormikProps<Department>) => (
                    <FormWrapper onSubmit={handleSubmit}>
                        <FormSection title="General">
                            <FormInput error={errors.name} icon={<Icon iconClass="font" type="fa" />} invalid={!!errors.name} label="Name" name="name" required={true} type="text" validated={submitCount > 0} value={values.name} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Job">
                            <FormSelect error={errors.companyId} invalid={!!errors.companyId} label="Company" name="companyId" options={companyOptions} required={true} theme="floating" value={values.companyId} onChange={setFieldValue} />
                        </FormSection>
                        <FormSection title="Employees">
                            <FieldArray name="employees">
                                {(arrayHelpers: FieldArrayRenderProps) => (
                                    <>
                                        <Table headers={[ "First name", "Last name", "Card number", "Phone number", "Remove" ]} required={[ true, true, false, false ]}>
                                            {values.employees && values.employees.map((employee: Employee, index: number) =>
                                                <EmployeeFields arrayHelpers={arrayHelpers} errors={errors} index={index} key={employee.id} setFieldValue={setFieldValue} submitCount={submitCount} values={values} onRemove={onRemove} />
                                            )}
                                        </Table>
                                        <Button text="+ Add Employee" theme="success" onClick={() => arrayHelpers.push(addEmployee())} />
                                    </>
                                )}
                            </FieldArray>
                        </FormSection>
                        <FormActions isSubmitting={isSubmitting} onCancel={onCancel} />
                    </FormWrapper>
                )}
            </Formik>
        </>
    );
};

export default EditDepartment;
