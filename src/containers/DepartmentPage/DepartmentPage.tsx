import { ArrayHelpers, FormikHelpers } from "formik";
import React, { useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps } from "react-router";
import { BreadcrumbEntry, FormOption, Page } from "smart-tech-solutions-frontend-components";
import { SweetAlertResult } from "sweetalert2";

import * as apiActions from "../../actions/apiActions";
import * as companyActions from "../../actions/companyActions";
import * as departmentActions from "../../actions/departmentActions";
import { showDeleteAlert } from "../../components/Alert";
import { getCompanyOptions } from "../../domain/company";
import { Department } from "../../domain/department";
import { RootState } from "../../reducers/initialState";

import EditDepartment from "./EditDepartment";
import ViewDepartment from "./ViewDepartment";

export type DepartmentPageProps = {
    companyId: string;
    departmentId: string;
    id: string;
};

const mapStateToProps = (state: RootState) => {
    return {
        companyList: state.company.companyList,
        department: state.department.department,
        errorMessages: state.api.errorMessages,
        loading: state.api.numberOfApiGetInProgress > 0,
        successMessages: state.api.successMessages,
        validationError: state.api.validationError
    };
};

const mapDispatchToProps = {
    clearValidationError: apiActions.clearValidationError,
    getCompanyList: companyActions.getCompanyList,
    getDepartment: departmentActions.getDepartment,
    removeErrorMessage: apiActions.removeErrorMessage,
    removeSuccessMessage: apiActions.removeSuccessMessage,
    saveDepartment: departmentActions.saveDepartment
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type AllDepartmentPageProps = RouteComponentProps<DepartmentPageProps> & PropsFromRedux;

const emptyDepartment: Department = {
    companyId: 0,
    employees: [],
    name: ""
};

const DepartmentPage = (props: AllDepartmentPageProps): JSX.Element => {
    const [ companyOptions, setCompanyOptions ] = useState<FormOption[]>([]);
    const [ department, setDepartment ] = useState<Department>(emptyDepartment);
    const [ editMode, setEditMode ] = useState<boolean>(false);

    useEffect(() => {
        props.getCompanyList();
    }, []);

    useEffect(() => {
        setCompanyOptions(getCompanyOptions(props.companyList));
    }, [ props.companyList ]);

    useEffect(() => {
        if (props.department) {
            setDepartment(props.department);
        }
    }, [ props.department ]);

    useEffect(() => {
        const params: URLSearchParams = new URLSearchParams(props.location.search);
        const mode: string | null = params.get("mode");
        const departmentId: number = parseInt(props.match.params.departmentId);
        const id: number = parseInt(props.match.params.id);
        props.clearValidationError();

        if (id) {
            props.getDepartment(id);
            mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
        } else if (departmentId) {
            props.getDepartment(departmentId);
            mode && mode === "edit" ? setEditMode(true) : setEditMode(false);
        } else {
            setDepartment(emptyDepartment);
            setEditMode(true);
        }
    }, [ props.match.params ]);

    const handleRemove = async (id: number, arrayHelpers: ArrayHelpers): Promise<void> => {
        const result: SweetAlertResult = await showDeleteAlert("remove");
        if (result.value) {
            arrayHelpers.remove(id);
            window.scrollTo(0, 0);
        }
    };

    const handleSubmit = async (department: Department, actions: FormikHelpers<Department>): Promise<void> => {
        const success: boolean = await props.saveDepartment(department);

        if (success) {
            actions.resetForm();
            props.clearValidationError();
            props.history.goBack();
        } else {
            window.scrollTo(0, 0);
        }
    };

    const handleCancel = (): void => {
        props.history.goBack();
    };

    const handleBack = (): void => {
        const departmentId: number = parseInt(props.match.params.departmentId);
        const id: number = parseInt(props.match.params.id);
        if (id) {
            props.history.push("/department-list");
        } else if (departmentId) {
            props.history.push("/company-list");
        }
    };

    const handleEdit = (): void => {
        if (props.match.params.departmentId) {
            props.history.push("/department/" + props.match.params.departmentId + "?mode=edit");
        } else {
            props.history.push("/department/" + props.match.params.id + "?mode=edit");
        }
    };

    const breadcrumbPage1: BreadcrumbEntry = { text: "Home", url: "/" };
    const breadcrumbPage2: BreadcrumbEntry = { text: "Departments", url: "/department-list" };
    const breadcrumbPage3: BreadcrumbEntry = { text: department.name, url: "/department/" + department.id };
    const breadcrumbPages: BreadcrumbEntry[] = [ breadcrumbPage1, breadcrumbPage2, breadcrumbPage3 ];

    return (
        <Page breadcrumbPages={breadcrumbPages} errorMessages={props.errorMessages} loading={props.loading} removeErrorMessage={props.removeErrorMessage} removeSuccessMessage={props.removeSuccessMessage} successMessages={props.successMessages} title={department.name}>
            {editMode ? <EditDepartment companyOptions={companyOptions} department={department} validationError={props.validationError} onCancel={handleCancel} onRemove={handleRemove} onSubmit={handleSubmit} /> : <ViewDepartment department={department} onBack={handleBack} onEdit={handleEdit} />}
        </Page>
    );
};

export default connector(DepartmentPage);
