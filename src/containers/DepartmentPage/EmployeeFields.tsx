import { ArrayHelpers, FormikErrors, getIn } from "formik";
import React from "react";
import { FormInput, Icon } from "smart-tech-solutions-frontend-components";

import RemoveButton from "../../components/RemoveButton";
import { Department } from "../../domain/department";

export type EmployeeFieldsProps = {
    arrayHelpers: ArrayHelpers;
    errors?: FormikErrors<Department>;
    index: number;
    onRemove: (id: number, arrayHelpers: ArrayHelpers) => void;
    setFieldValue: (name: string, value: string | number | boolean | null | undefined) => void;
    submitCount: number;
    values: Department;
};

const DepartmentFields = ({ arrayHelpers, errors, index, onRemove, setFieldValue, submitCount, values }: EmployeeFieldsProps): JSX.Element => {

    return (
        <tr key={index}>
            <td>
                <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!getIn(errors, "employees." + index + ".firstName")} name={`employees.${index}.firstName`} theme="no-margins" type="text" validated={submitCount > 0} value={values.employees && values.employees[index].firstName} onChange={setFieldValue} />
            </td>
            <td>
                <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!getIn(errors, "employees." + index + ".lastName")} name={`employees.${index}.lastName`} theme="no-margins" type="text" validated={submitCount > 0} value={values.employees && values.employees[index].lastName} onChange={setFieldValue} />
            </td>
            <td>
                <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!getIn(errors, "employees." + index + ".cardId")} name={`employees.${index}.cardId`} theme="no-margins" type="text" validated={submitCount > 0} value={values.employees && values.employees[index].cardId} onChange={setFieldValue} />
            </td>
            <td>
                <FormInput icon={<Icon iconClass="font" type="fa" />} invalid={!!getIn(errors, "employees." + index + ".phone")} name={`employees.${index}.phone`} theme="no-margins" type="text" validated={submitCount > 0} value={values.employees && values.employees[index].phone} onChange={setFieldValue} />
            </td>
            {values.employees && values.employees[index].id ? <RemoveButton arrayHelpers={arrayHelpers} id={index} onRemove={onRemove} /> : <RemoveButton arrayHelpers={arrayHelpers} id={index} onRemove={() => {
                arrayHelpers.remove(index);
            }} />}
        </tr>
    );
};

export default DepartmentFields;
