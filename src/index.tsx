import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Store } from "redux";

import App from "./App";
import LoginPage from "./containers/LoginPage/LoginPage";
import configureStore from "./store/configureStore";

const store: Store = configureStore();

render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route component={LoginPage} path="/login" exact />
                <Route path="/">
                    <App />
                </Route>
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById("app")
);
