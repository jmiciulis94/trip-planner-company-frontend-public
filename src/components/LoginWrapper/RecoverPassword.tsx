import React from "react";

import "./LoginWrapper.css";

const RecoverPassword = (): JSX.Element => {

    return (
        <>
            <div className="form-group">
                <div className="col-xs-12">
                    <h3>Recover Password</h3>
                    <p className="text-muted">Enter your Email and instructions will be sent to you! </p>
                </div>
            </div>
            <div className="form-group">
                <div className="col-xs-12">
                    <input className="form-control" placeholder="Email" type="text" required />
                </div>
            </div>
            <div className="form-group text-center mt-3">
                <div className="col-xs-12">
                    <button className="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                </div>
            </div>
        </>
    );
};

export default RecoverPassword;
