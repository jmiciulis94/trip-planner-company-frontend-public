import React, { ReactNode } from "react";
import Card from "react-bootstrap/Card";

import "./LoginWrapper.css";

export type LoginProps = {
    backgroundImagePath: string;
    children: ReactNode;
};

const LoginWrapper = ({ backgroundImagePath, children }: LoginProps): JSX.Element => {

    return (
        <div className="sts-login-register" style={{ backgroundImage: `url('${backgroundImagePath}')` }}>
            <Card className="sts-login-box sts-card">
                <Card.Body>
                    {children}
                </Card.Body>
            </Card>
        </div>
    );
};

export default LoginWrapper;
