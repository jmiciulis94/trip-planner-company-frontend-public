import React from "react";
import Form from "react-bootstrap/Form";

import "./LoginWrapper.css";

const SignUp = (): JSX.Element => {

    return (
        <Form.Group className="mb-0">
            <div className="col-sm-12 text-center">
                <p>Don&apos;t have an account? <a className="text-info ml-1" href="register"><b>Sign Up</b></a></p>
            </div>
        </Form.Group>
    );
};

export default SignUp;
