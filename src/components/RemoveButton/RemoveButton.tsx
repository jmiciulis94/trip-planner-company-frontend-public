import { ArrayHelpers } from "formik";
import React from "react";
import { Button } from "smart-tech-solutions-frontend-components";

import "./RemoveButton.css";

export type RemoveButtonProps = {
    arrayHelpers: ArrayHelpers;
    id: number;
    onRemove: (id: number, arrayHelpers: ArrayHelpers) => void;
};

const RemoveButton = ({ arrayHelpers, id, onRemove }: RemoveButtonProps): JSX.Element => (
    <td className="sts-remove-button-column">
        <Button text="remove" theme="delete" onClick={() => onRemove(id, arrayHelpers)} />
    </td>
);

export default RemoveButton;
