import Swal, { SweetAlertResult } from "sweetalert2";

const DeleteAlert = Swal.mixin({
    buttonsStyling: false,
    customClass: {
        confirmButton: "btn btn-danger"
    }
});

export const showDeleteAlert = (theme: string): Promise<SweetAlertResult> => {
    switch (theme) {
        case "remove":
            return DeleteAlert.fire({
                confirmButtonText: "Yes, remove it!",
                icon: "warning",
                text: "You are trying to remove an existing item.",
                title: "Are you sure?"
            });
        default:
            return DeleteAlert.fire({
                confirmButtonText: "Yes, delete it!",
                icon: "warning",
                text: "You won't be able to revert this!",
                title: "Are you sure?"
            });
    }
};
