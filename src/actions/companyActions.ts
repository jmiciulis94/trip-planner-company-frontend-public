import { Dispatch } from "redux";

import { Company } from "../domain/company";

import { Action, CREATE_COMPANY, DELETE_COMPANY, deleteItem, GET_COMPANY, GET_COMPANY_LIST, getItem, getItemsList, saveItem, UPDATE_COMPANY } from "./action";

export const getCompanySuccess = (company: Company): Action<Company> => {
    return {
        data: company,
        type: GET_COMPANY
    };
};

export const getCompanyListSuccess = (companyList: Company[]): Action<Company[]> => {
    return {
        data: companyList,
        type: GET_COMPANY_LIST
    };
};

export const createCompanySuccess = (company: Company): Action<Company> => {
    return {
        data: company,
        type: CREATE_COMPANY
    };
};

export const updateCompanySuccess = (company: Company): Action<Company> => {
    return {
        data: company,
        type: UPDATE_COMPANY
    };
};

export const deleteCompanySuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_COMPANY
    };
};

export const getCompany = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/company/" + id, getCompanySuccess);
};

export const getCompanyList = (): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/company", getCompanyListSuccess);
};

export const saveCompany = (company: Company): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/company", company, company.id ? updateCompanySuccess : createCompanySuccess, "Company successfully saved.");
};

export const deleteCompany = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/company/" + id, id, deleteCompanySuccess, "Company successfully deleted.");
};
