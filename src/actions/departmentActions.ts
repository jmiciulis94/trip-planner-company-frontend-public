import { Dispatch } from "redux";

import { Department } from "../domain/department";

import { Action, CREATE_DEPARTMENT, DELETE_DEPARTMENT, deleteItem, GET_DEPARTMENT, GET_DEPARTMENT_LIST, getItem, getItemsList, saveItem, UPDATE_DEPARTMENT } from "./action";

export const getDepartmentSuccess = (department: Department): Action<Department> => {
    return {
        data: department,
        type: GET_DEPARTMENT
    };
};

export const getDepartmentListSuccess = (departmentList: Department[]): Action<Department[]> => {
    return {
        data: departmentList,
        type: GET_DEPARTMENT_LIST
    };
};

export const createDepartmentSuccess = (department: Department): Action<Department> => {
    return {
        data: department,
        type: CREATE_DEPARTMENT
    };
};

export const updateDepartmentSuccess = (department: Department): Action<Department> => {
    return {
        data: department,
        type: UPDATE_DEPARTMENT
    };
};

export const deleteDepartmentSuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_DEPARTMENT
    };
};

export const getDepartment = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/department/" + id, getDepartmentSuccess);
};

export const getDepartmentList = (): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/department", getDepartmentListSuccess);
};

export const saveDepartment = (department: Department): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/department", department, department.id ? updateDepartmentSuccess : createDepartmentSuccess, "Department successfully saved.");
};

export const deleteDepartment = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/department/" + id, id, deleteDepartmentSuccess, "Department successfully deleted.");
};
