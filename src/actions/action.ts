import { Dispatch } from "redux";

import { ajaxDelete, ajaxGet, ajaxPost, ApiResponse } from "../utils/apiUtils";

import { addErrorMessage, addSuccessMessage, beginApiCall, endApiCall, setValidationError } from "./apiActions";

export const BEGIN_API_CALL = "BEGIN_API_CALL";
export const END_API_CALL = "END_API_CALL";
export const ADD_SUCCESS_MESSAGE = "ADD_SUCCESS_MESSAGE";
export const REMOVE_SUCCESS_MESSAGE = "REMOVE_SUCCESS_MESSAGE";
export const ADD_ERROR_MESSAGE = "ADD_ERROR_MESSAGE";
export const REMOVE_ERROR_MESSAGE = "REMOVE_ERROR_MESSAGE";
export const SET_VALIDATION_ERROR = "SET_VALIDATION_ERROR";
export const CLEAR_VALIDATION_ERROR = "CLEAR_VALIDATION_ERROR";

export const GET_ACCOUNT = "GET_ACCOUNT";
export const GET_ACCOUNT_LIST = "GET_ACCOUNT_LIST";
export const CREATE_ACCOUNT = "CREATE_ACCOUNT";
export const UPDATE_ACCOUNT = "UPDATE_ACCOUNT";
export const DELETE_ACCOUNT = "DELETE_ACCOUNT";

export const GET_COMPANY = "GET_COMPANY";
export const GET_COMPANY_LIST = "GET_COMPANY_LIST";
export const CREATE_COMPANY = "CREATE_COMPANY";
export const UPDATE_COMPANY = "UPDATE_COMPANY";
export const DELETE_COMPANY = "DELETE_COMPANY";

export const GET_DEPARTMENT = "GET_DEPARTMENT";
export const GET_DEPARTMENT_LIST = "GET_DEPARTMENT_LIST";
export const CREATE_DEPARTMENT = "CREATE_DEPARTMENT";
export const UPDATE_DEPARTMENT = "UPDATE_DEPARTMENT";
export const DELETE_DEPARTMENT = "DELETE_DEPARTMENT";

export const GET_EMPLOYEE = "GET_EMPLOYEE";
export const GET_EMPLOYEE_LIST = "GET_EMPLOYEE_LIST";
export const CREATE_EMPLOYEE = "CREATE_EMPLOYEE";
export const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
export const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";

export type Action<T> = {
    data?: T;
    type: string;
};

export type Message = {
    id: number;
    message: string;
};

const createErrorMessage = <T> (response: ApiResponse<T>): Message => {
    const id = new Date().getTime();
    return {
        id: id,
        message: response.error ? response.error.header : "Server error. Please try again later."
    };
};

const createSuccessMessage = (message: string): Message => {
    const id = new Date().getTime();
    return {
        id: id,
        message: message
    };
};

export const getItem = <T> (url: string, successFunction: (item: T) => Action<T>) => {
    return async function (dispatch: Dispatch): Promise<void> {
        dispatch(beginApiCall());
        const response: ApiResponse<T> = await ajaxGet<T>(url);
        dispatch(endApiCall());

        if (response.status === 200 && response.data) {
            dispatch(successFunction(response.data));
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
        }
    };
};

export const getItemsList = <T> (url: string, successFunction: (items: T[]) => Action<T[]>) => {
    return async function (dispatch: Dispatch): Promise<void> {
        dispatch(beginApiCall());
        const response: ApiResponse<T[]> = await ajaxGet<T[]>(url);
        dispatch(endApiCall());

        if (response.status === 200 && response.data) {
            dispatch(successFunction(response.data));
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
        }
    };
};

export const saveItem = <T> (url: string, item: T, successFunction: (item: T) => Action<T>, successMessage: string) => {
    return async function (dispatch: Dispatch): Promise<boolean> {
        dispatch(beginApiCall());
        const response: ApiResponse<T> = await ajaxPost<T>(url, item);
        dispatch(endApiCall());

        if (response.status === 200 && response.data) {
            dispatch(successFunction(response.data));
            dispatch(addSuccessMessage(createSuccessMessage(successMessage)));
            return true;
        } else if (response.status === 400 && response.validationError) {
            dispatch(setValidationError(response.validationError));
            return false;
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
            return false;
        }
    };
};

export const deleteItem = (url: string, id: number, successFunction: (id: number) => Action<number>, successMessage: string) => {
    return async function (dispatch: Dispatch): Promise<void> {
        dispatch(beginApiCall());
        const response: ApiResponse<void> = await ajaxDelete(url);
        dispatch(endApiCall());

        if (response.status === 204) {
            dispatch(successFunction(id));
            dispatch(addSuccessMessage(createSuccessMessage(successMessage)));
        } else {
            dispatch(addErrorMessage(createErrorMessage(response)));
        }
    };
};
