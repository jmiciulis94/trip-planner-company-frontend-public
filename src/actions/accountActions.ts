import { Dispatch } from "redux";

import { Account } from "../domain/account";

import { Action, CREATE_ACCOUNT, DELETE_ACCOUNT, deleteItem, GET_ACCOUNT, GET_ACCOUNT_LIST, getItem, getItemsList, saveItem, UPDATE_ACCOUNT } from "./action";

export const getAccountSuccess = (account: Account): Action<Account> => {
    return {
        data: account,
        type: GET_ACCOUNT
    };
};

export const getAccountListSuccess = (accountList: Account[]): Action<Account[]> => {
    return {
        data: accountList,
        type: GET_ACCOUNT_LIST
    };
};

export const createAccountSuccess = (account: Account): Action<Account> => {
    return {
        data: account,
        type: CREATE_ACCOUNT
    };
};

export const updateAccountSuccess = (account: Account): Action<Account> => {
    return {
        data: account,
        type: UPDATE_ACCOUNT
    };
};

export const deleteAccountSuccess = (id: number): Action<number> => {
    return {
        data: id,
        type: DELETE_ACCOUNT
    };
};

export const getAccount = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return getItem(process.env.API_URL + "/account/" + id, getAccountSuccess);
};

export const getAccountList = (): (dispatch: Dispatch) => Promise<void> => {
    return getItemsList(process.env.API_URL + "/account", getAccountListSuccess);
};

export const saveAccount = (account: Account): (dispatch: Dispatch) => Promise<boolean> => {
    return saveItem(process.env.API_URL + "/account", account, account.id ? updateAccountSuccess : createAccountSuccess, "Account successfully saved.");
};

export const deleteAccount = (id: number): (dispatch: Dispatch) => Promise<void> => {
    return deleteItem(process.env.API_URL + "/account/" + id, id, deleteAccountSuccess, "Account successfully deleted.");
};
