import React from "react";
import { Icon, Level1MenuItem, MenuGroupLabel, MenuSeparator, SideBarMenuWrapper } from "smart-tech-solutions-frontend-components";

const AppMenu = (): JSX.Element => (
    <SideBarMenuWrapper>
        <MenuSeparator />
        <MenuGroupLabel text="MAIN" />
        <Level1MenuItem icon={<Icon iconClass="view-grid" type="mdi" />} text="Dashboard" url="/dashboard" />
        <MenuSeparator />
        <MenuGroupLabel text="LISTS" />
        <Level1MenuItem icon={<Icon iconClass="table" type="mdi" />} text="Company" url="/company-list" />
        <Level1MenuItem icon={<Icon iconClass="view-grid" type="mdi" />} text="Department" url="/department-list" />
        <Level1MenuItem icon={<Icon iconClass="human-male-female" type="mdi" />} text="Employee" url="/employee-list" />
    </SideBarMenuWrapper>
);

export default AppMenu;
