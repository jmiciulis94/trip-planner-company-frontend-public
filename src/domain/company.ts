import { FormOption } from "smart-tech-solutions-frontend-components";

import { Department } from "./department";

export type Company = {
    address: string;
    city: string;
    companyCode: number | string;
    departments?: Department[];
    email: string;
    id?: number;
    name: string;
    phone: string;
    postalCode?: string;
    vatCode: number | string;
};

export const fullAddress = (address: string, city: string): string => {
    return address + ", " + city;
};

export const getCompanyOptions = (companyList: Company[]): FormOption[] => {
    return companyList.map((company: Company) => {
        return {
            label: company.name,
            value: company.id || ""
        };
    });
};
