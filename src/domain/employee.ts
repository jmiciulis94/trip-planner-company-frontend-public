export type Employee = {
    cardId?: number;
    departmentId: number;
    departmentName?: string;
    firstName: string;
    id?: number;
    lastName: string;
    phone?: string;
};

export const fullName = (firstName: string, lastName?: string | undefined): string => {
    return lastName ? firstName + " " + lastName : firstName;
};
