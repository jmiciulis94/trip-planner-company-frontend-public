export type Account = {
    avatar?: string;
    email: string;
    firstName?: string;
    id?: number;
    lastName?: string;
    password: string;
    phone?: string;
    role?: string;
};

export const fullName = (firstName: string, lastName?: string | undefined): string => {
    return lastName ? firstName + " " + lastName : firstName;
};