import { FormOption } from "smart-tech-solutions-frontend-components";

import { Employee } from "./employee";

export type Department = {
    companyId: number;
    companyName?: string;
    employees?: Employee[];
    id?: number;
    name: string;
};

export const getDepartmentOptions = (departmentList: Department[]): FormOption[] => {
    return departmentList.map((department: Department) => {
        return {
            label: department.name,
            value: department.id || ""
        };
    });
};
