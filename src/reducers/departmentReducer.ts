import { Action, CREATE_DEPARTMENT, DELETE_DEPARTMENT, GET_DEPARTMENT, GET_DEPARTMENT_LIST, UPDATE_DEPARTMENT } from "../actions/action";
import { Department } from "../domain/department";
import { DepartmentState } from "../store/state";

import initialState from "./initialState";

export default function departmentReducer (state: DepartmentState = initialState.department, action: Action<Department | Department[] | number>): DepartmentState {
    switch (action.type) {
        case GET_DEPARTMENT: {
            const receivedDepartment: Department = action.data as Department;
            return { ...state, department: receivedDepartment };
        }
        case GET_DEPARTMENT_LIST: {
            const receivedDepartmentList: Department[] = action.data as Department[];
            return { ...state, departmentList: receivedDepartmentList };
        }
        case CREATE_DEPARTMENT: {
            const createdDepartment: Department = action.data as Department;
            return { ...state, departmentList: state.departmentList.concat(createdDepartment) };
        }
        case UPDATE_DEPARTMENT: {
            const updatedDepartment: Department = action.data as Department;
            return { ...state, departmentList: state.departmentList.map((d: Department) => d.id === updatedDepartment.id ? updatedDepartment : d) };
        }
        case DELETE_DEPARTMENT: {
            const id: number = action.data as number;
            return { ...state, departmentList: state.departmentList.filter((d: Department) => d.id !== id) };
        }
        default:
            return state;
    }
}
