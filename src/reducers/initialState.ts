import { Message } from "../actions/action";
import { Account } from "../domain/account";
import { Company } from "../domain/company";
import { Department } from "../domain/department";
import { Employee } from "../domain/employee";
import { ValidationError } from "../utils/apiUtils";

export type RootState = {
    account: {
        account?: Account;
        accountList: Account[]
    };
    api: {
        errorMessages: Message[];
        numberOfApiGetInProgress: number;
        successMessages: Message[];
        validationError?: ValidationError;
    };
    company: {
        company?: Company;
        companyList: Company[]
    },
    department: {
        department?: Department;
        departmentList: Department[];
    },
    employee: {
        employee?: Employee;
        employeeList: Employee[];
    };
};

const initialState: RootState = {
    account: {
        accountList: []
    },
    api: {
        errorMessages: [],
        numberOfApiGetInProgress: 0,
        successMessages: []
    },
    company: {
        companyList: []
    },
    department: {
        departmentList: []
    },
    employee: {
        employeeList: []
    }
};

export default initialState;
