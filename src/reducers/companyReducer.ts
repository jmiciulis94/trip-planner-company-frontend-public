import { Action, CREATE_COMPANY, DELETE_COMPANY, GET_COMPANY, GET_COMPANY_LIST, UPDATE_COMPANY } from "../actions/action";
import { Company } from "../domain/company";
import { CompanyState } from "../store/state";

import initialState from "./initialState";

export default function companyReducer (state: CompanyState = initialState.company, action: Action<Company | Company[] | number>): CompanyState {
    switch (action.type) {
        case GET_COMPANY: {
            const receivedCompany: Company = action.data as Company;
            return { ...state, company: receivedCompany };
        }
        case GET_COMPANY_LIST: {
            const receivedCompanyList: Company[] = action.data as Company[];
            return { ...state, companyList: receivedCompanyList };
        }
        case CREATE_COMPANY: {
            const createdCompany: Company = action.data as Company;
            return { ...state, companyList: state.companyList.concat(createdCompany) };
        }
        case UPDATE_COMPANY: {
            const updatedCompany: Company = action.data as Company;
            return { ...state, companyList: state.companyList.map((c: Company) => c.id === updatedCompany.id ? updatedCompany : c) };
        }
        case DELETE_COMPANY: {
            const id: number = action.data as number;
            return { ...state, companyList: state.companyList.filter((c: Company) => c.id !== id) };
        }
        default:
            return state;
    }
}
