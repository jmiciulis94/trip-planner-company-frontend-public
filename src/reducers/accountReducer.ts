import { Action, CREATE_ACCOUNT, DELETE_ACCOUNT, GET_ACCOUNT, GET_ACCOUNT_LIST, UPDATE_ACCOUNT } from "../actions/action";
import { Account } from "../domain/account";
import { AccountState } from "../store/state";

import initialState from "./initialState";

export default function accountReducer (state: AccountState = initialState.account, action: Action<Account | Account[] | number>): AccountState {
    switch (action.type) {
        case GET_ACCOUNT: {
            const receivedAccount: Account = action.data as Account;
            return { ...state, account: receivedAccount };
        }
        case GET_ACCOUNT_LIST: {
            const receivedAccountList: Account[] = action.data as Account[];
            return { ...state, accountList: receivedAccountList };
        }
        case CREATE_ACCOUNT: {
            const createdAccount: Account = action.data as Account;
            return { ...state, accountList: state.accountList.concat(createdAccount) };
        }
        case UPDATE_ACCOUNT: {
            const updatedAccount: Account = action.data as Account;
            return { ...state, accountList: state.accountList.map((a: Account) => a.id === updatedAccount.id ? updatedAccount : a) };
        }
        case DELETE_ACCOUNT: {
            const id: number = action.data as number;
            return { ...state, accountList: state.accountList.filter((a: Account) => a.id !== id) };
        }
        default:
            return state;
    }
}
