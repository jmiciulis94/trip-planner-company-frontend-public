import { Action, CREATE_EMPLOYEE, DELETE_EMPLOYEE, GET_EMPLOYEE, GET_EMPLOYEE_LIST, UPDATE_EMPLOYEE } from "../actions/action";
import { Employee } from "../domain/employee";
import { EmployeeState } from "../store/state";

import initialState from "./initialState";

export default function employeeReducer (state: EmployeeState = initialState.employee, action: Action<Employee | Employee[] | number>): EmployeeState {
    switch (action.type) {
        case GET_EMPLOYEE: {
            const receivedEmployee: Employee = action.data as Employee;
            return { ...state, employee: receivedEmployee };
        }
        case GET_EMPLOYEE_LIST: {
            const receivedEmployeeList: Employee[] = action.data as Employee[];
            return { ...state, employeeList: receivedEmployeeList };
        }
        case CREATE_EMPLOYEE: {
            const createdEmployee: Employee = action.data as Employee;
            return { ...state, employeeList: state.employeeList.concat(createdEmployee) };
        }
        case UPDATE_EMPLOYEE: {
            const updatedEmployee: Employee = action.data as Employee;
            return { ...state, employeeList: state.employeeList.map((e: Employee) => e.id === updatedEmployee.id ? updatedEmployee : e) };
        }
        case DELETE_EMPLOYEE: {
            const id: number = action.data as number;
            return { ...state, employeeList: state.employeeList.filter((e: Employee) => e.id !== id) };
        }
        default:
            return state;
    }
}
