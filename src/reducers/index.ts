import { combineReducers } from "redux";

import account from "./accountReducer";
import api from "./apiReducer";
import company from "./companyReducer";
import department from "./departmentReducer";
import employee from "./employeeReducer";

const rootReducer = combineReducers({
    account,
    api,
    company,
    department,
    employee
});

export default rootReducer;
